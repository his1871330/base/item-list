import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-item-body',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './item-body.component.html',
  styleUrls: ['./item-body.component.scss']
})
export class ItemBodyComponent {

}
